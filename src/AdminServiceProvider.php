<?php 

namespace Kmc\AdminAppPackage;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('admin', function ($app) {
            return new admin;
        });

        // Files to publish

        // CSS, Images, JS, Fonts
        $this->publishes([
            __DIR__.'/assets' => public_path(),
        ], 'public');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        // Loading route file
        require __DIR__ . '/Http/routes.php';

        // Loading view file
        $this->loadViewsFrom(__DIR__ . '/views', 'admin');
    }
}
