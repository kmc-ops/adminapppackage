<?php 

namespace Kmc\AdminAppPackage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traffic;

class DashController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
		
		#Caching traffic data of this page
		Traffic::cache(7,$this->user->employee_id,NULL,'/application/7','Admin App Home Page','apple-mobile',date('Y-m-d H:i:s'));
		
		return view('admin::dash.index');
	
	}

}
