<?php

Route::group(['prefix' => 'application/7'], function () {
  
    Route::get('/', [
        'as' => 'dash', 'uses' => 'Kmc\AdminAppPackage\Http\Controllers\DashController@index'
    ]);

	Route::resource('/employees', 'Kmc\AdminAppPackage\Http\Controllers\EmployeeController');

});

