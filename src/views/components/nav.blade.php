<nav class="bar bar-tab">

    <a class="tab-item" href="{{ route('dash') }}">
        <span class="icon icon-home"></span>
        <span class="tab-label">Home</span>
    </a>

    <a class="tab-item" href="{{ route('application.7.employees.index') }}">
        <span class="icon fa fa-group"></span>
        <span class="tab-label">Employees</span>
    </a>

</nav>
