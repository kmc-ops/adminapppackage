@extends('admin::layout.master')

@section('topbar')

	@include('admin::dash.sub.topbar')

@stop

@section('content')

	<ul class="table-view">
		<li class="table-view-cell">
			<a class="navigate-right">
			<span class="badge">5</span>
			Item 1
			</a>	
		</li>
		<li class="table-view-cell">
			<a class="navigate-right">
			<span class="badge">5</span>
			Item 2
			</a>
		</li>
	</ul>

@stop

@section('post-content')
	@include('admin::components.nav')
@stop
