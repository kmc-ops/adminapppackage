<!DOCTYPE html>
<html> 

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="Cache-control" content="private">
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="initial-scale=1.0, user-scalable=0"/>
		<meta name="apple-mobile-web-app-capable" content="yes"/>
		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<title>KMC | Admin</title>

		<!-- Ratchet -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ratchet/2.0.2/css/ratchet.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<!-- Custom -->
		<link rel="stylesheet" media="all and (orientation:portrait)" type="text/css" href="{{ asset('/admin/css/portrait.css') }}">
		<link rel="stylesheet" media="all and (orientation:landscape)" type="text/css" href="{{ asset('/admin/css/landscape.css') }}">

		<!--Import jQuery -->
		<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
		<!-- Pace -->
		<script data-pace-options='{ "ajax": true }' src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/pink/pace-theme-flash.css">
		
		@yield('styles')
		<style>
			.noselect {
				-webkit-touch-callout: none;
				-webkit-user-select: none;
				-khtml-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
			.btn-link, .icon-close, .media-object {
				color: #e90f92 !important;
			}
			.tab-item:active {
				color: #e90f92 !important;
			}
			.field-value {
				padding-top:0 !important;
				padding-bottom:5px !important;
				padding-right:15px !important;
			}
			.field-title {
				color:#6E4D9B;
				padding-left:15px !important;
			}
		</style>

	</head>

	<body>
		<div class="loader" style="height:100%; width:100%; position:absolute; display:none; background-color:rgba(0,0,0,0.3); z-index:999;"></div>

		@yield('topbar')

		@yield('pre-content')

		<div class="content" style="top:-16px;">

			@yield('content')	

		</div>

		@yield('post-content')

		@yield('modals')

		<!-- Ratchet -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/ratchet/2.0.2/js/ratchet.min.js"></script>

		<script>

		/*$('.ajax').on('click', function() {
			var route = $(this).attr('route');
			Pace.start();
			$.ajax({
				url: route,
				async: true,
				type: "GET", // not POST, laravel won't allow it
				success: function(data) {
				  	$topbar = $(data.topbar);
				  	$precontent = $(data.precontent);
				  	$content = $(data.content);

				  	$('.bar-nav').html($topbar);    
				  	$('.pre-content').html($precontent);    
				  	$('.content').html($content);    
				},
				error: function(error) {
					console.log(error);
				}
			});
			Pace.stop();
		});*/

		</script>
			
		<!-- Google Analytics -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-90780071-4', 'auto');
		ga('send', 'pageview');

		</script>

		@yield('scripts')

	</body>

</html>
