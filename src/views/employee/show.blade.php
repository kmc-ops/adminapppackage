@extends('admin::layout.master')

@section('topbar')
	
	<header class="bar bar-nav">
		<a class="btn btn-link btn-nav pull-left" href="{{ URL::previous() }}">
			<span class="icon icon-left-nav"></span>
			Back
		</a>
		<a class="btn btn-link btn-nav pull-right" href="#modalEditEmployee">
			<span class="icon icon-compose"></span>
		</a>
		<h1 class="title"> {{ $employee->first_name }} {{ $employee->last_name }} </h1>
	</header>

@stop

@section('content')

	<ul class="table-view">
		<li class="table-view-cell media">
			<img class="media-object pull-left" src="http://shackmanlab.org/wp-content/uploads/2013/07/person-placeholder.jpg" style="border-radius:25px; height:50px; width:50px;">
			<div class="media-body">
				{{ $employee->position }}
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor sit amet.</p>
			</div>
		</li>

		<span class="badge badge-inverted field-title">primary</span>
		<li class="table-view-cell field-value">
			{{ "(".substr($employee->phone_1, 0, 3).") ".substr($employee->phone_1, 3, 3)."-".substr($employee->phone_1,6) }}
			<div class="pull-right">
				<a class="icon fa fa-phone media-object" href="tel:{{$employee->phone_1}}"></a>
				&nbsp;
				<a class="icon fa fa-comments-o media-object" href="tel:{{$employee->phone_1}}"></a>
			</div>
		</li>

		<li class="table-view-cell table-view-divider">Divider</li>
		<li class="table-view-cell">Item 3</li>
		<li class="table-view-cell">Item 4</li>
	</ul>

@stop

@section('modals')

    <div id="modalEditEmployee" class="modal">
        <header class="bar bar-nav">
			<a class="btn btn-link btn-nav pull-left" href="#modalEditEmployee">
				&nbsp;Cancel
			</a>
			<a class="btn btn-link btn-nav pull-right" href="#modalEditEmployee">
				Save&nbsp;
			</a>
            <h1 class="title">{{ $employee->first_name }} {{ $employee->last_name }}</h1>
        </header>

        <div class="content">
            <p>test</p>
        </div>
    </div>

@stop
