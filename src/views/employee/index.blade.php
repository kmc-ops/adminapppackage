@extends('admin::layout.master')

@section('topbar')

	@include('admin::employee.sub.topbar')

@stop

@section('content')

	<ul class="table-view">

		@foreach($employees as $idx=>$employee)

			@if($idx == 0)
				<li class="table-view-cell table-view-divider" style="color:#e90f92;">{{ strtoupper($employee->first_name[0]) }}</li>
			@elseif( isset($employees[$idx - 1]) && strtoupper($employee->first_name[0]) != strtoupper($employees[$idx-1]->first_name[0]) )
				<li class="table-view-cell table-view-divider" style="color:#e90f92;">{{ strtoupper($employee->first_name[0]) }}</li>
			@else
			@endif

			<li class="table-view-cell">
				<a class="navigate-right" href="{{ route('application.7.employees.show', $employee->id) }}">
				<strong>{{ $employee->first_name }}</strong> {{ $employee->last_name }}
				</a> 
			</li>

		@endforeach

	</ul>

@stop

@section('post-content')
	@include('admin::components.nav')
@stop

@section('modals')

	<div id="modalCreateEmployee" class="modal">
		<header class="bar bar-nav">
			<a class="icon icon-close pull-right" href="#modalCreateEmployee"></a>
			<h1 class="title">New Employee</h1>
		</header>

		<div class="content">
			<p>test</p>	
		</div>
	</div>

@stop
